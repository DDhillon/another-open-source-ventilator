# Another Open Source Ventilator

A Low Cost/Resource Fully Featured Ventilator. Features An Assist Mode, Variable Respiratory Rate, Pressure Limiting, Tidal Volume, I:E Ratio And Plateau Pressure. The Design Uses Few Components And Displays Similar Data To A Commercial Machine.